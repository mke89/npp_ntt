#ifndef DATARECEIVERIMPL_H
#define DATARECEIVERIMPL_H

#include "IDataReceiver.h"

class DataReceiverImpl: public IDataReceiver
{
    public:
        DataReceiverImpl();
        virtual void DataBlockReceived(const void* data_ptr, uint32_t data_size, const uint32_t file_index) override;
        virtual void DataReceivingStopped() override;
        virtual ~DataReceiverImpl();
};

#endif // DATARECEIVERIMPL_H

#include <iostream>
#include <sstream>
#include <thread>
#include <chrono>

#include "FilesReader.h"

FilesReader::FilesReader(const std::vector<std::string> &filesList,
                         IDataReceiver *dataReceiver):
    filesList(filesList),
    dataReceiver(dataReceiver),
    initialiseFlg(false),
    commandSent(false),
    offset(0),
    controlCommand(NONE),
    currErrState(ErrorState()),
    opState(OperationState())
{
    operationFinished.store(false);
    threadStarted.store(false);
    stopCommand.store(Command::NONE);
    buffer = std::shared_ptr<char>(new char[MAX_BLOCK_SIZE + 100],
                                   std::default_delete<char[]>());
    initialise();
}

FilesReader::~FilesReader()
{
}

bool FilesReader::finished()
{
    return operationFinished.load();
}

bool FilesReader::isInitialised()
{
    return initialiseFlg;
}

bool FilesReader::incomingDataCorrect()
{
    if(!dataReceiver)
    {
        std::lock_guard<std::mutex> errStateMutexLocker(errorStateMutex);
        currErrState = ErrorState{RECEIVER_IS_NULLPTR};
        return false;
    }

    if(filesList.empty())
    {
        std::lock_guard<std::mutex> errStateMutexLocker(errorStateMutex);
        currErrState = ErrorState{FILES_LIST_IS_EMPTY};
        return false;
    }

    if(filesList.size() > MAX_FILES_COUNT)
    {
        std::lock_guard<std::mutex> errStateMutexLocker(errorStateMutex);
        currErrState = ErrorState{FILES_LIST_TOO_BIG};
        return false;
    }

    return true;
}

bool FilesReader::oneOrMoreFilesEmpty(std::vector<std::shared_ptr<FILE>> &filesHandlers)
{
    for(size_t currFile = 0; currFile < filesHandlers.size(); currFile++)
    {
        FILE* fileDescr = filesHandlers[currFile].get();

        if(fseek(fileDescr, 0, SEEK_END) != 0)
        {
            std::lock_guard<std::mutex> errStateMutexLocker(errorStateMutex);
            currErrState = ErrorState{FILE_SEEK_ERROR, (int)currFile,
                                      ferror(fileDescr)};
            return true;
        }

        long int fileSizeBytes = ftell(fileDescr);
        if(fileSizeBytes < 0)
        {
            std::lock_guard<std::mutex> errStateMutexLocker(errorStateMutex);
            currErrState = ErrorState{FILE_SIZE_ERROR, (int)currFile,
                                      ferror(fileDescr)};
            return true;
        }
        filesSizesBytes[currFile] = fileSizeBytes;

        if(fseek(fileDescr, 0, SEEK_SET) != 0)
        {
            std::lock_guard<std::mutex> errStateMutexLocker(errorStateMutex);
            currErrState = ErrorState{FILE_SEEK_ERROR, (int)currFile,
                                      ferror(fileDescr)};
            return true;
        }
    }

    return false;
}

void FilesReader::initialise()
{
    if(!incomingDataCorrect())
    {
        return;
    }

    filesHandlers.resize(filesList.size());
    filesSizesBytes.resize(filesList.size());
    chunksSizes.resize(filesList.size());

    for(size_t currentFile = 0; currentFile < filesList.size(); currentFile++)
    {
        const char* fileName = filesList[currentFile].c_str();

        std::shared_ptr<FILE> fileHandler(fopen(fileName, "rb"), fclose);
        if(!fileHandler.get())
        {
            std::lock_guard<std::mutex> errStateMutexLocker(errorStateMutex);
            currErrState = ErrorState{ONE_OF_FILES_OPENING_ERROR, (int)currentFile};
            return;
        }
        filesHandlers[currentFile] = std::move(fileHandler);
    }

    if(oneOrMoreFilesEmpty(filesHandlers))
    {
        return;
    }

    initialiseFlg = true;
}

bool FilesReader::canPerformCommand()
{
    if(!isInitialised())
    {
        std::lock_guard<std::mutex> errStateMutexLocker(errorStateMutex);
        currErrState = ErrorState{NOT_INITIALISED};
        return false;
    }

    if(!threadStarted.load())
    {
        std::lock_guard<std::mutex> errStateMutexLocker(errorStateMutex);
        currErrState = ErrorState{THREAD_NOT_STARTED};
        return false;
    }

    return true;
}

bool FilesReader::start()
{
    if(!canPerformCommand())
    {
        return false;
    }

    OperationState operationState;
    std::unique_lock<std::mutex> opStateMutexLocker(operationStateMutex);
    operationState = opState;
    opStateMutexLocker.unlock();

    if(!((operationState.state == State::READY_TO_WORK) && (!operationState.operationFinished)))
    {
        if(operationState.state == State::RUNNING)
        {
            std::lock_guard<std::mutex> errStateMutexLocker(errorStateMutex);
            currErrState = ErrorState{ALREADY_STARTED};
        }
        else
        {
            std::lock_guard<std::mutex> errStateMutexLocker(errorStateMutex);
            currErrState = ErrorState{IMPOSSIBLE_TO_START};
        }
        return false;
    }

    std::lock_guard<std::mutex> mutexLock(controlMutex);
    offset = 0;
    commandSent = true;
    controlCommand = Command::START;
    syncVar.notify_one();

    return true;
}

bool FilesReader::startFrom(uint32_t offset)
{
    if(!canPerformCommand())
    {
        return false;
    }

    OperationState operationState;
    std::unique_lock<std::mutex> opStateMutexLocker(operationStateMutex);
    operationState = opState;
    opStateMutexLocker.unlock();

    if(!((operationState.state == State::READY_TO_WORK) && (!operationState.operationFinished)))
    {
        if(operationState.state == State::RUNNING)
        {
            std::lock_guard<std::mutex> errStateMutexLocker(errorStateMutex);
            currErrState = ErrorState{ALREADY_STARTED};
        }
        else
        {
            std::lock_guard<std::mutex> errStateMutexLocker(errorStateMutex);
            currErrState = ErrorState{IMPOSSIBLE_TO_START};
        }
        return false;
    }

    std::lock_guard<std::mutex> mutexLock(controlMutex);
    this->offset = offset;
    commandSent = true;
    controlCommand = Command::START_FROM;
    syncVar.notify_one();

    return true;
}

bool FilesReader::stop()
{
    if(!canPerformCommand())
    {
        return false;
    }

    OperationState operationState;
    std::unique_lock<std::mutex> opStateMutexLocker(operationStateMutex);
    operationState = opState;
    opStateMutexLocker.unlock();

    if(!((operationState.state == State::RUNNING) && (!operationState.operationFinished)))
    {
        std::lock_guard<std::mutex> errStateMutexLocker(errorStateMutex);
        currErrState = ErrorState{ALREADY_STOPPED};
        return false;
    }

    stopCommand.store(Command::STOP);
    return true;
}

bool FilesReader::stopThread()
{
    if(!canPerformCommand())
    {
        return false;
    }

    OperationState operationState;
    std::unique_lock<std::mutex> opStateMutexLocker(operationStateMutex);
    operationState = opState;
    opStateMutexLocker.unlock();

    if(opState.operationFinished)
    {
        std::lock_guard<std::mutex> errStateMutexLocker(errorStateMutex);
        currErrState = ErrorState{ALREADY_STOPPED};
        return false;
    }

    stopCommand.store(Command::STOP);

    std::lock_guard<std::mutex> mutexLock(controlMutex);
    this->offset = 0;
    commandSent = true;
    controlCommand = Command::STOP_THREAD;
    syncVar.notify_one();

    return true;
}

bool FilesReader::pause()
{
    if(!canPerformCommand())
    {
        return false;
    }

    OperationState operationState;
    std::unique_lock<std::mutex> opStateMutexLocker(operationStateMutex);
    operationState = opState;
    opStateMutexLocker.unlock();

    if(!((operationState.state == State::RUNNING) && (!operationState.operationFinished)))
    {
        if(operationState.state == State::PAUSED)
        {
            std::lock_guard<std::mutex> errStateMutexLocker(errorStateMutex);
            currErrState = ErrorState{ALREADY_PAUSED};
        }
        else
        {
            std::lock_guard<std::mutex> errStateMutexLocker(errorStateMutex);
            currErrState = ErrorState{ALREADY_STOPPED};
        }

        return false;
    }

    stopCommand.store(Command::PAUSE);
    return true;
}

bool FilesReader::resume()
{
    if(!canPerformCommand())
    {
        return false;
    }

    OperationState operationState;
    std::unique_lock<std::mutex> opStateMutexLocker(operationStateMutex);
    operationState = opState;
    opStateMutexLocker.unlock();

    if(!((operationState.state == State::PAUSED) && (!operationState.operationFinished)))
    {
        if(operationState.state == State::RUNNING)
        {
            std::lock_guard<std::mutex> errStateMutexLocker(errorStateMutex);
            currErrState = ErrorState{ALREADY_RESUMED};
        }
        else
        {
            std::lock_guard<std::mutex> errStateMutexLocker(errorStateMutex);
            currErrState = ErrorState{IMPOSSIBLE_TO_RESUME};
        }

        return false;
    }

    std::lock_guard<std::mutex> mutexLock(controlMutex);
    offset = 0;
    commandSent = true;
    controlCommand = Command::RESUME;
    syncVar.notify_one();

    return true;
}

bool FilesReader::resumeFrom(uint32_t offset)
{
    if(!canPerformCommand())
    {
        return false;
    }

    OperationState operationState;
    std::unique_lock<std::mutex> opStateMutexLocker(operationStateMutex);
    operationState = opState;
    opStateMutexLocker.unlock();

    if(!((operationState.state == State::PAUSED) && (!operationState.operationFinished)))
    {
        if(operationState.state == State::RUNNING)
        {
            std::lock_guard<std::mutex> errStateMutexLocker(errorStateMutex);
            currErrState = ErrorState{ALREADY_RESUMED};
        }
        else
        {
            std::lock_guard<std::mutex> errStateMutexLocker(errorStateMutex);
            currErrState = ErrorState{IMPOSSIBLE_TO_RESUME};
        }

        return false;
        return false;
    };

    std::lock_guard<std::mutex> mutexLock(controlMutex);
    this->offset = offset;
    commandSent = true;
    controlCommand = Command::RESUME_FROM;
    syncVar.notify_one();

    return true;
}

FilesReader::OperationState FilesReader::getOperationState()
{
    OperationState operationState;
    std::lock_guard<std::mutex> opStateMutexLocker(operationStateMutex);
    operationState = opState;

    return operationState;
}

std::string FilesReader::getStateString()
{
    OperationState opState = getOperationState();

    std::stringstream output;
    output << "State: ";

    if(opState.state == State::READY_TO_WORK)
    {
        output << "READY_TO_WORK;   ";
    }
    else if(opState.state == State::STOPPED)
    {
        output << "STOPPED;   ";
    }
    else if(opState.state == State::PAUSED)
    {
        output << "PAUSED;   ";
    }
    else if(opState.state == State::RUNNING)
    {
        output << "RUNNING;   ";
    }

    output << "Operation: ";
    if(opState.operationFinished)
    {
        output << "Finished";
    }
    else
    {
        output << "Not finished";
    }

    return output.str();
}

FilesReader::ErrorState FilesReader::getLastError()
{
    ErrorState errState;
    std::lock_guard<std::mutex> errStateMutexLocker(errorStateMutex);
    errState = currErrState;
    return errState;
}

bool FilesReader::readChunksSizes()
{
    for(size_t currFile = 0; currFile < filesHandlers.size(); currFile++)
    {
        uint32_t chunkSize;
        FILE* fileDescr = filesHandlers[currFile].get();
        size_t elementsRead = fread(&chunkSize, sizeof(uint32_t), 1, fileDescr);
        if(elementsRead != 1)
        {
            std::lock_guard<std::mutex> errStateMutexLocker(errorStateMutex);
            currErrState = ErrorState{FILE_READ_ERROR, (int)currFile, ferror(fileDescr)};
            return false;
        }

        if(chunkSize > MAX_BLOCK_SIZE)
        {
            std::lock_guard<std::mutex> errStateMutexLocker(errorStateMutex);
            currErrState = ErrorState{DATA_BLOCK_TOO_BIG, (int)currFile};
            return false;
        }

        chunksSizes[currFile] = chunkSize;
    }

    return true;
}

bool FilesReader::readAndProcessNextDataChunks()
{
    if(!readChunksSizes())
    {
        return false;
    }

    for(size_t currFile = 0; currFile < filesHandlers.size(); currFile++)
    {
        uint32_t chunkSize = chunksSizes[currFile];
        FILE* fileDescr = filesHandlers[currFile].get();
        size_t bytesRead = fread(buffer.get(), sizeof(char), chunkSize, fileDescr);
        if(bytesRead != chunkSize)
        {
            std::lock_guard<std::mutex> errStateMutexLocker(errorStateMutex);
            currErrState = ErrorState{FILE_READ_ERROR, (int)currFile, ferror(fileDescr)};
            return false;
        }

        dataReceiver->DataBlockReceived(buffer.get(), chunkSize, (uint32_t)currFile);
    }

    return true;
}

bool FilesReader::findSuitableOffset(uint32_t *correctedOffset, uint32_t givenOffset, int fileIndex)
{
    FILE* fileDescr = filesHandlers[fileIndex].get();
    if(givenOffset >= filesSizesBytes[fileIndex])
    {
        std::lock_guard<std::mutex> errStateMutexLocker(errorStateMutex);
        currErrState = ErrorState{FILE_OFFSET_TOO_BIG, (int)fileIndex};
        return false;
    }

    long int currentOffset = ftell(fileDescr);
    if(currentOffset != 0)
    {
        std::lock_guard<std::mutex> errStateMutexLocker(errorStateMutex);
        currErrState = ErrorState{FILE_SEEK_ERROR, (int)fileIndex,
                                  ferror(fileDescr)};
        return false;
    }

    while(((uint32_t)currentOffset <= givenOffset) || (!feof(fileDescr)))
    {
        uint32_t chunkSize;
        size_t bytesRead = fread(&chunkSize, sizeof(uint32_t), 1, fileDescr);
        if(bytesRead != 1)
        {
            std::lock_guard<std::mutex> errStateMutexLocker(errorStateMutex);
            currErrState = ErrorState{FILE_READ_ERROR, (int)fileIndex, ferror(fileDescr)};
            return false;
        }

        if(chunkSize > MAX_BLOCK_SIZE)
        {
            std::lock_guard<std::mutex> errStateMutexLocker(errorStateMutex);
            currErrState = ErrorState{FILE_OFFSET_TOO_BIG, (int)fileIndex};
            return false;
        }

        long int newOffset = currentOffset + chunkSize + HEADER_SIZE;

        if((uint32_t)newOffset == givenOffset)
        {
            *correctedOffset = newOffset;
            return true;
        }

        if((uint32_t)newOffset > givenOffset)
        {
            *correctedOffset = currentOffset;
            return true;
        }

        if(fseek(fileDescr, chunkSize, SEEK_CUR) != 0)
        {
            std::lock_guard<std::mutex> errStateMutexLocker(errorStateMutex);
            currErrState = ErrorState{FILE_SEEK_ERROR, (int)fileIndex,
                                      ferror(fileDescr)};
            return false;
        }

        currentOffset = newOffset;
    }

    std::lock_guard<std::mutex> errStateMutexLocker(errorStateMutex);
    currErrState = ErrorState{FILE_OFFSET_TOO_BIG, (int)fileIndex};

    return false;
}

bool FilesReader::performFilesOffset(const std::vector<uint32_t> &offsets)
{
    for(size_t currFile = 0; currFile < filesHandlers.size(); currFile++)
    {
        FILE* fileDescr = filesHandlers[currFile].get();

        if(fseek(fileDescr, offsets[currFile], SEEK_SET) != 0)
        {
            std::lock_guard<std::mutex> errStateMutexLocker(errorStateMutex);
            currErrState = ErrorState{FILE_SEEK_ERROR, (int)currFile,
                                      ferror(fileDescr)};
            return false;
        }
    }

    return true;
}

bool FilesReader::performOffsetFilePointers(uint32_t offset)
{
    std::vector<uint32_t> offsets(filesHandlers.size(), 0);

    if(!performFilesOffset(offsets))
    {
        return false;
    }

    if(offset != 0)
    {
        for(size_t currFile = 0; currFile < filesHandlers.size(); currFile++)
        {
            uint32_t correctedOffset = 0;
            if(!findSuitableOffset(&correctedOffset, offset, (int)currFile))
            {
                return false;
            }
            offsets[currFile] = correctedOffset;
        }

        if(!performFilesOffset(offsets))
        {
            return false;
        }
    }

    return true;
}

bool FilesReader::performReading()
{
    bool finishReading = false;
    while(true)
    {
        //std::this_thread::sleep_for(std::chrono::milliseconds(1000));

        if(!readAndProcessNextDataChunks())
        {
            finishReading = true;
            break;
        }

        int stopCmd = stopCommand.load();
        if(stopCmd == Command::STOP)
        {
            stopCommand.store(Command::NONE);
            finishReading = true;
            break;
        }
        else if(stopCmd == Command::PAUSE)
        {
            stopCommand.store(Command::NONE);
            std::unique_lock<std::mutex> opStateMutexLocker(operationStateMutex);
            opState.state = State::PAUSED;
            opStateMutexLocker.unlock();
            break;
        }
    }

    return finishReading;
}

void FilesReader::read()
{
    if(!initialiseFlg)
    {
        return;
    }

    threadStarted.store(true);

    while(true)
    {
        int currentCommand;
        uint32_t currentOffset = 0;
        std::unique_lock<std::mutex> mutexLock(controlMutex);
        syncVar.wait(mutexLock, [this]() {return commandSent;});
        currentOffset = offset;
        commandSent = false;
        currentCommand = controlCommand;
        mutexLock.unlock();

        OperationState operationState;
        std::unique_lock<std::mutex> opStateMutexLocker(operationStateMutex);
        operationState = opState;
        opStateMutexLocker.unlock();

        if((currentCommand == Command::START) || (currentCommand == Command::RESUME))
        {
            std::unique_lock<std::mutex> opStateMutexLocker(operationStateMutex);
            opState.state = State::RUNNING;
            opStateMutexLocker.unlock();

            bool finishReading = performReading();
            if(finishReading)
            {
                break;
            }
        }
        else if((currentCommand == Command::START_FROM) || (currentCommand == Command::RESUME_FROM))
        {
            std::unique_lock<std::mutex> opStateMutexLocker(operationStateMutex);
            opState.state = State::RUNNING;
            opStateMutexLocker.unlock();

            if(!performOffsetFilePointers(currentOffset))
            {
                break;
            }

            bool finishReading = performReading();
            if(finishReading)
            {
                break;
            }
        }
        else if(currentCommand == Command::STOP_THREAD)
        {
            break;
        }
    }

    std::unique_lock<std::mutex> opStateMutexLocker(operationStateMutex);
    opState.state = STOPPED;
    opState.operationFinished = true;
    opStateMutexLocker.unlock();
    dataReceiver->DataReceivingStopped();
}

std::string FilesReader::ErrorState::toString()
{
    std::stringstream output;
    output << "readerOpErrCode: " << readerOpErrCode << "   "
           << "fileNum: " << fileNum << "   "
           << "fileErrNo: " << fileErrNo << std::endl;

    return output.str();
}

FilesReader::ErrorState::ErrorState(Error readerErrCode,
                                    int fileNum,
                                    int fileErrNo):
    readerOpErrCode(readerErrCode),
    fileNum(fileNum),
    fileErrNo(fileErrNo)
{
}


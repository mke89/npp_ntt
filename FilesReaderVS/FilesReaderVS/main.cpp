#include <iostream>
#include <cstdlib>
#include <cstring>
#include <thread>
#include <chrono>

#include "FilesReader.h"
#include "DataReceiverImpl.h"

bool argumentsCountCorrect(int argsCount);
void printHelp();
bool processCommand(int cmd, FilesReader &filesReader);


int main(int argc, char *argv[])
{
    std::vector<std::string> files;

    if(!argumentsCountCorrect(argc))
    {
        return 0;
    }

    for(int currFile = 1; currFile < argc; currFile++)
    {
        files.push_back(std::string(argv[currFile]));
    }

    DataReceiverImpl dataReceiver;
    FilesReader filesReader(files, &dataReceiver);

    if(!filesReader.isInitialised())
    {
        std::cout << "Error: reader not initialised!" << std::endl;
        std::cout << "Error info: " << filesReader.getLastError().toString() << std::endl;
        return 0;
    }

    std::thread readerThread(&FilesReader::read, std::ref(filesReader));

    while(true)
    {
        printHelp();
        int cmd;
        std::cin >> cmd;

        if(processCommand(cmd, filesReader))
        {
            break;
        }
    }

    readerThread.join();

    return 0;
}


bool argumentsCountCorrect(int argsCount)
{
    if(argsCount < 2)
    {
        std::cout << "Error: Files list not set." << std::endl;
        return false;
    }

    if(argsCount > 21)
    {
        std::cout << "Error: Files list exceed maximum number." << std::endl;
        return false;
    }

    return true;
}


void printHelp()
{
    std::cout << "1. Start" << std::endl;
    std::cout << "2. Start from..." << std::endl;
    std::cout << "3. Pause" << std::endl;
    std::cout << "4. Resume" << std::endl;
    std::cout << "5. Resume from..." << std::endl;
    std::cout << "6. Stop" << std::endl;
    std::cout << "7. Stop thread" << std::endl;
    std::cout << "8. State" << std::endl;
    std::cout << "9. Error state" << std::endl;
    std::cout << "10. Exit" << std::endl;
}


bool processCommand(int cmd, FilesReader &filesReader)
{
    bool exit = false;
    switch(cmd)
    {
        case 1:
        {
            filesReader.start();
            break;
        }

        case 2:
        {
            uint32_t offset = 0;
            std::cout << "Enter offset > ";
            std::cin >> offset;
            filesReader.startFrom(offset);
            break;
        }

        case 3:
        {
            filesReader.pause();
            break;
        }

        case 4:
        {
            filesReader.resume();
            break;
        }

        case 5:
        {
            uint32_t offset = 0;
            std::cout << "Enter offset > ";
            std::cin >> offset;
            filesReader.resumeFrom(offset);
            break;
        }

        case 6:
        {
            filesReader.stop();
            break;
        }

        case 7:
        {
            filesReader.stopThread();
            break;
        }

        case 8:
        {
            std::cout << filesReader.getStateString() << std::endl << std::endl;
            break;
        }

        case 9:
        {
            std::cout << "Error state: " << filesReader.getLastError().toString()
                      << std::endl << std::endl;
            break;
        }

        case 10:
        {
            if (!filesReader.getOperationState().operationFinished)
            {
                filesReader.stopThread();
            }

            exit = true;
            break;
        }
    }

    return exit;
}


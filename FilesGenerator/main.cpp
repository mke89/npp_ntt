#include <iostream>
#include <cstdlib>
#include <cstring>
#include <ctime>

using namespace std;

// FilesGenerator fileName blocksCount maxBlockSize
int getRandomNumber(int min, int max);

int main(int argc, char *argv[])
{
    if(argc < 4)
    {
        cout << "Error: not all arguments are given";
        return 1;
    }

    const char* fileName = argv[1];
    uint32_t blocksCount = atoi(argv[2]);
    uint32_t maxBlockSize = atoi(argv[3]);

    cout << "fileName: " << fileName << endl;
    cout << "blocksCount: " << blocksCount << endl;
    cout << "maxBlockSize: " << maxBlockSize << endl;
    cout << "Generating..." << endl;

    FILE *fp;
    if((fp = fopen(fileName, "wb")) == nullptr)
    {
        cout << "Error: cannot open file";
        return 1;
    }

    char buffer[maxBlockSize];
    memset(buffer, 0xFF, maxBlockSize);
    srand(static_cast<unsigned int>(time(0)));
    for(uint32_t currentBlock = 0; currentBlock < blocksCount; currentBlock++)
    {
        uint32_t currentBlockSize = getRandomNumber(1, maxBlockSize);
        uint32_t preambleSize = sizeof(uint32_t);

        size_t dataWritten = fwrite(&currentBlockSize, 1, sizeof(uint32_t), fp);
        if(dataWritten != preambleSize)
        {
            cout << "Error: cannot write block preamble";
            fclose(fp);
            return 1;
        }

        dataWritten = fwrite(buffer, sizeof(char), currentBlockSize, fp);
        if(dataWritten != currentBlockSize)
        {
            cout << "Error: cannot write data block";
            fclose(fp);
            return 1;
        }
    }

    fclose(fp);

    cout << "Done!";
}


int getRandomNumber(int min, int max)
{
    static const double fraction = 1.0 / (static_cast<double>(RAND_MAX) + 1.0);
    // Равномерно распределяем рандомное число в нашем диапазоне
    return static_cast<int>(rand() * fraction * (max - min + 1) + min);
}

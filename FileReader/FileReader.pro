QT -= gui

CONFIG += c++11 console
CONFIG -= app_bundle


SOURCES += \
        DataReceiverImpl.cpp \
        FilesReader.cpp \
        main.cpp

HEADERS += \
    DataReceiverImpl.h \
    FilesReader.h \
    IDataReceiver.h


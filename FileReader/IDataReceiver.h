#ifndef IDATARECEIVER_H
#define IDATARECEIVER_H

#include <cstdint>

class IDataReceiver
{
    public:
        virtual void DataBlockReceived(const void* data_ptr, uint32_t data_size, const uint32_t file_index) = 0;
        virtual void DataReceivingStopped() = 0;
        virtual ~IDataReceiver() {}

};

#endif // IDATARECEIVER_H

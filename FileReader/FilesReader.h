#ifndef FILESREADER_H
#define FILESREADER_H

#include <vector>
#include <string>
#include <condition_variable>
#include <mutex>
#include <cstdio>
#include <atomic>

#include "IDataReceiver.h"

class FilesReader
{
    public:
        enum Params
        {
            MAX_FILES_COUNT = 20,
            MAX_BLOCK_SIZE  = 65535,
            HEADER_SIZE     = sizeof(uint32_t),
        };

        enum State
        {
            READY_TO_WORK,
            STOPPED,
            RUNNING,
            PAUSED,
        };

        enum Command
        {
            NONE,
            START,
            START_FROM,
            PAUSE,
            RESUME,
            RESUME_FROM,
            STOP,
            STOP_THREAD,
        };

        enum Error
        {
            OK,
            ALREADY_STARTED,            // Чтение уже запущено
            IMPOSSIBLE_TO_START,        // Невозможно запустить чтение
            ALREADY_STOPPED,            // Чтение уже остановлено
            ALREADY_PAUSED,             // Чтение уже приостановлено
            ALREADY_RESUMED,            // Чтение уже возобновлено
            IMPOSSIBLE_TO_RESUME,       // Невозможно возобновить чтение
            FILES_LIST_IS_EMPTY,        // Пустой список с файлами
            FILES_LIST_TOO_BIG,         // Список файлов превышает допустимый размер
            RECEIVER_IS_NULLPTR,        // Указатель на приемника не задан
            ONE_OF_FILES_OPENING_ERROR, // Ошибка при открытии одного из фалов
            ONE_OF_FILES_IS_EMPTY,      // Один из фалов пустой
            FILE_READ_ERROR,            // Ошибка при чтении файла
            FILE_SEEK_ERROR,            // Ошибка перемещения указателя по файлу
            FILE_SIZE_ERROR,            // Ошибка определения размера файла
            FILE_IS_EOF,                // Достигнут конец файла
            FILE_OFFSET_TOO_BIG,        // Смещение вышло за границу файла
            DATA_BLOCK_TOO_BIG,         // Размер блока данных превышает максимально допустимый размер
            THREAD_NOT_STARTED,         // Поток чтения не запущен
            NOT_INITIALISED,            // Объект не инициализирован
        };

        struct ErrorState
        {
            Error readerOpErrCode;
            int fileNum;
            int fileErrNo;

            ErrorState(Error readerErrCode = Error::OK, int fileNum = -1, int fileErrNo = 0);
            std::string toString();
        };

        struct OperationState
        {
            State state;
            bool operationFinished;

            OperationState(State state = State::READY_TO_WORK, bool finished = false):
                state(state),
                operationFinished(finished)
            {}
        };


        FilesReader(const std::vector<std::string> &filesList,
                    IDataReceiver *dataReceiver);
        FilesReader(const FilesReader&) = delete;
        void operator=(const FilesReader&) = delete;
        ~FilesReader();

        bool isInitialised();
        bool start();
        bool startFrom(uint32_t offset);
        bool stop();
        bool stopThread();
        bool pause();
        bool resume();
        bool resumeFrom(uint32_t offset);
        void read();
        OperationState getOperationState();
        std::string getStateString();
        ErrorState getLastError();

    private:
        std::vector<std::string> filesList;

        IDataReceiver *dataReceiver;
        bool initialiseFlg;
        bool commandSent;
        uint32_t offset;
        int controlCommand;
        ErrorState currErrState;
        OperationState opState;
        std::mutex errorStateMutex;
        std::mutex controlMutex;
        std::mutex operationStateMutex;
        std::condition_variable syncVar;
        std::vector<std::shared_ptr<FILE>> filesHandlers;
        std::vector<uint32_t> filesSizesBytes;
        std::vector<uint32_t> chunksSizes;
        std::atomic<int> stopCommand;
        std::atomic<bool> operationFinished;
        std::atomic<bool> threadStarted;
        std::shared_ptr<char> buffer;

        bool findSuitableOffset(uint32_t *correctedOffset,
                                uint32_t givenOffset,
                                int fileIndex);
        bool performFilesOffset(const std::vector<uint32_t> &offsets);
        void initialise();
        bool incomingDataCorrect();
        bool oneOrMoreFilesEmpty(std::vector<std::shared_ptr<FILE>> &filesHandlers);
        bool readChunksSizes();
        bool readAndProcessNextDataChunks();
        bool performOffsetFilePointers(uint32_t offset);
        bool finished();
        bool performReading();
        bool canPerformCommand();
};

#endif // FILESREADER_H
